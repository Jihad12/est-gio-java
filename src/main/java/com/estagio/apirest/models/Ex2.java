package com.estagio.apirest.models;



import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/maiuscula")
public class Ex2 {


	@GetMapping("/{words}")
	public ResponseEntity<List<String>> execute(@RequestParam List<String> words) {
		
		for (int i = 0; i<words.size();i++) {
			words.set(i, words.get(i).toUpperCase());
		}
		
		   
		return new ResponseEntity<>(words, HttpStatus.OK);
	
	}
}