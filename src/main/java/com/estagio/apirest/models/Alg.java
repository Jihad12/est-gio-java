package com.estagio.apirest.models;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/alg")
public class Alg {


	@GetMapping("/{saque}")
	public ResponseEntity<String> execute(@PathVariable double saque) {
		
		int nota5 = 0;
		int nota3 = 0;
		
		while(saque%5 != 0) {
			nota3++;
			saque = saque - 3;		
		}
		nota5 = (int) saque/5;
		return new ResponseEntity<>(String.format("Notas de 3 = %d, Notas de 5 = %d", nota3, nota5), HttpStatus.OK);

		 
	}
}


	