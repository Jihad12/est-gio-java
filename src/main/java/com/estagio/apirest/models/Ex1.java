package com.estagio.apirest.models;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/contapalavra")
public class Ex1 {


	@GetMapping("/{word}")
	public ResponseEntity<String> execute(@PathVariable String word) {
		
		
		return new ResponseEntity<>(String.format("%s, tamanho = %d", word, word.length()), HttpStatus.OK);
		}
		 
	}

	